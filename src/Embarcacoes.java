
public class Embarcacoes {
	private int portaAviao;				// 5 posicoes
	private int navioTanque;			// 4 posicoes
	private int contraTorpedeiro;		// 3 posicoes
	private int submarino;				// 2 posicoes
	private int hidroaviao;				// 1 posicao
	
	public Embarcacoes() {
		this.portaAviao =  0;
		this.navioTanque = 0;
		this.contraTorpedeiro = 0;
		this.submarino = 0;
		this.hidroaviao = 0;
	}
	
	public int getQtdEmbarcacoes() {
		int total = portaAviao + navioTanque + contraTorpedeiro + submarino + hidroaviao;
		return total;
	}
	
	public int getQtdCelulas() {
		int total = (5*portaAviao) + (4*navioTanque) + (3*contraTorpedeiro) + (2*submarino) + hidroaviao;
		return total;
	}
	
	public void setPortaAviao(int qtdPortaAvioes) {
		this.portaAviao = qtdPortaAvioes;
	}
	public int getPortaAviao() {
		return this.portaAviao;
	}
	public void setNavioTanque(int qtdNaviosTanque) {
		this.navioTanque = qtdNaviosTanque;
	}
	public int getNavioTanque() {
		return this.navioTanque;
	}
	public void setContraTorpedeiro(int qtdContraTorp) {
		this.contraTorpedeiro = qtdContraTorp;
	}
	public int getContraTorpedeiro() {
		return this.contraTorpedeiro;
	}
	public void setSubmarino(int qtdSubmarinos) {
		this.submarino = qtdSubmarinos;
	}
	public int getSubmarino() {
		return this.submarino;
	}
	public void setHidroAviao(int qtdSubmarinos) {
		this.hidroaviao = qtdSubmarinos;
	}
	public int getHidroAviao() {
		return this.hidroaviao;
	}
}

