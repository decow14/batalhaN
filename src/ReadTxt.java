import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class ReadTxt {
	private int Linhas;
	private int Colunas;
	private int[][] Matriz;
	private Embarcacoes barcos = new Embarcacoes();
 
  public ReadTxt(int numeroMapa) {
	boolean arqExiste = false;
	while (!arqExiste) {
		String nome = "maps/map_" + (numeroMapa) + ".txt";
	    try {
	      FileReader arq = new FileReader(nome);
	      BufferedReader lerArq = new BufferedReader(arq);
	      int numLinha = 0;
	      String linha = lerArq.readLine(); 
	      while (linha != null) {
	    	if (numLinha == 1) {
	    		 setLinhasColunas(linha);
	    	} 
	        linha = lerArq.readLine(); 
	        numLinha += 1;
	      }
	      arq.close();
	      
	     arq = new FileReader(nome);
	     lerArq = new BufferedReader(arq);
	     linha = lerArq.readLine(); 
	     numLinha = 0;
	     Matriz = new int[Linhas][Colunas];
	     while (linha != null) {
		 	if ((numLinha >=4) && (numLinha <= Linhas+4)) {
		 		 setMatriz(linha, numLinha);
		 	}
		 	if (numLinha >= Linhas+6) {
		 		setQtdEmbarcacoes(linha);
		 	}
		 	linha = lerArq.readLine(); 
		    numLinha += 1;
	     }
	     arq.close();
	     arqExiste = true;
	     
	    } catch (IOException e) {
	        System.err.printf("Erro na abertura do arquivo: %s.\n",
	          e.getMessage());
	    }
	    System.out.println();
	}
  }
  
  private void setMatriz(String linha, int numLinha) {
	  for(int cont = 0; cont < linha.length(); cont++){  
	        char letra = linha.charAt(cont); 
	        String aux = "" + letra;
	       Matriz[numLinha-4][cont] = strToInt(aux, 0);
	}
  }
  
  private void setQtdEmbarcacoes(String linha) {
	  String linhaTemp = "";
	  int tamEmbarcacao = 0;
	  int qtdEmbarcao = 0;
	  for(int cont = 0; cont < linha.length(); cont++){  
  	        char letra = linha.charAt(cont);  
  	        if (letra != ' ') {
  	        	linhaTemp = linhaTemp + letra;
  	        }else {
  	        	tamEmbarcacao = strToInt(linhaTemp, 5);
  	        	linhaTemp = "";
  	        }
  	  }
	  qtdEmbarcao = strToInt(linhaTemp, 5);
	  if (tamEmbarcacao == 1) {
		  barcos.setHidroAviao(qtdEmbarcao);
	  }else if(tamEmbarcacao == 2) {
		  barcos.setSubmarino(qtdEmbarcao);
	  }else if(tamEmbarcacao == 3) {
		  barcos.setContraTorpedeiro(qtdEmbarcao);
	  }else if(tamEmbarcacao == 4) {
		  barcos.setNavioTanque(qtdEmbarcao);
	  }else if(tamEmbarcacao == 5) {
		  barcos.setPortaAviao(qtdEmbarcao);
	  }
  }
    
  private void setLinhasColunas(String linha) {
	  String linhaTemp = "";
	  for(int cont = 0; cont < linha.length(); cont++){  
  	        char letra = linha.charAt(cont);  
  	        if (letra != ' ') {
  	        	linhaTemp = linhaTemp + letra;
  	        }else {
  	        	Colunas = strToInt(linhaTemp, 5);
  	        	linhaTemp = "";
  	        }
  	  }
	  Linhas = strToInt(linhaTemp, 5);
  }
  
  public static int strToInt(String valor, int padrao) 
  {
     try {
         return Integer.valueOf(valor); 
     } 
     catch (NumberFormatException e) { 
         return padrao;
     }
  }
  
  public int getLinhas() {
	  return this.Linhas;
  }
  
  public int getColunas () {
	  return this.Colunas;
  }
  
  public int[][] getMatriz(){
	  return this.Matriz;
  }
  
  public Embarcacoes getEmbarcacoes() {
	return barcos;  
  }

}
