
public class Habilidade {
	private boolean selecionada;
	private int preco;
	private int ID;
	private String nome;
	
	public Habilidade(int ID, String nome, int preco, boolean selecionada) {
		this.nome = nome;
		this.ID = ID;
		this.preco = preco;
		this.selecionada = selecionada;
	}
	
	public void setSelecionado(boolean selecao) {
		this.selecionada = selecao;
	}
	
	public boolean getSelecionada() {
		return this.selecionada;
	}
	public int getPreco() {
		return this.preco;
	}
	public String getNome() {
		return this.nome;
	}
	public int getID() {
		return this.ID;
	}
}
