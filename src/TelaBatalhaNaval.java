import java.awt.Canvas;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;

import javax.swing.ImageIcon;
import javax.swing.JFrame;

public class TelaBatalhaNaval extends JFrame{
	
	/**
	 * 
	 */
	
	
	
	
	private static final long serialVersionUID = 1L;
	public static final int LARGURA_CELULA = 60;
	public static final int ALTURA_CELULA = 60;
	public static final int LARGURA_BOTAO = 60;
	public static final int ALTURA_BOTAO = 60;
	public static final int LARGURA_PAINELHABILIDADES = 300;
	public static final int LARGURA_PAINEL_USER = 200;
	public static final int SALDO_INICIAL = 300;
	
	private TelaDificuldade telaDif;
	public desenhaTela Fundo;
	private CanvasThread atualizaTelaThread;
	
	private Jogador player;
	private ReadTxt map;
	private Tabuleiro tabuleiro;
	private Habilidades habilidades;
	
	public TelaBatalhaNaval() {
			Fundo = new desenhaTela();
			atualizaTelaThread  = new CanvasThread(this);
			System.out.println("Criando jogo");
	}
	
	private void voltaTela() {
		this.setVisible(false);
		atualizaTelaThread.setRunning(false);
		telaDif.setVisible(true);
	}
	
	public void criaTela(TelaDificuldade telaAnterior) {
		
		telaDif = telaAnterior;
		player = telaDif.player;
		map = telaDif.map;
		tabuleiro = new Tabuleiro(map.getLinhas(),  map.getColunas());
		habilidades = new Habilidades(TelaBatalhaNaval.SALDO_INICIAL);
		tabuleiro.setMatrizNavios(map.getMatriz());

		this.setTitle("Batalha Naval");
		setSize(TelaBatalhaNaval.LARGURA_CELULA * map.getColunas()+TelaBatalhaNaval.LARGURA_PAINELHABILIDADES+TelaBatalhaNaval.LARGURA_PAINEL_USER, TelaBatalhaNaval.ALTURA_CELULA * map.getLinhas()+20);
		this.setLocationRelativeTo(null);
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		this.setResizable(false);
		this.getContentPane().add("Center", Fundo);
		this.setVisible(true);
		atualizaTelaThread.setRunning(true);
		atualizaTelaThread.start();
		
		Fundo.addMouseMotionListener(new MouseMotionListener() {
			@Override
			public void mouseMoved(MouseEvent e) {	
				int x=e.getX();
				int y=e.getY();
		    
				if (habilidades.HabilidadeSelecionada() != null) {
					boolean possuiSaldo = habilidades.getSaldoJogo().possuiSaldo(habilidades.HabilidadeSelecionada().getPreco());
					if ((x > LARGURA_PAINEL_USER && x < LARGURA_CELULA*map.getColunas()+LARGURA_PAINEL_USER) && possuiSaldo){
						
						int x_pos = (x-LARGURA_PAINEL_USER)/LARGURA_CELULA;
						int y_pos = y/ALTURA_CELULA;
						
						if (habilidades.IDHabilidadeSelecionada() == Habilidades.ID_ATACAR_UMA_POSICAO) {
							tabuleiro.miraCelula(y_pos, x_pos);
						}else if (habilidades.IDHabilidadeSelecionada() == Habilidades.ID_ATACAR_AREA_2x2) {
							tabuleiro.miraCelula2x2(y_pos, x_pos);
						}else if (habilidades.IDHabilidadeSelecionada() == Habilidades.ID_ATACAR_LINHA_COLUNA) {
			       			tabuleiro.miraLinhaColuna(y_pos, x_pos);
		        		}
			     	}
				}
			}
			@Override
			public void mouseDragged(MouseEvent e) {				
			
			}
		});

		
		Fundo.addMouseListener(new MouseListener() {
			
		@Override
		public void mouseReleased(MouseEvent e) {
			 int x=e.getX();
		     int y=e.getY();		
			     
		     if ((x > 0 && x < 200) && (y > 0 && y < 40)) {
		    	 voltaTela();
		     }
			     
		     int minXBotoes = LARGURA_PAINEL_USER+(map.getColunas()*LARGURA_CELULA)+30;
		     int maxXBotoes = LARGURA_PAINEL_USER+(map.getColunas()*LARGURA_CELULA)+70;
			     
		     if (x > minXBotoes && x < maxXBotoes) {
		    	 if  (y > 35 && y < 70) {
		    		 habilidades.marcaHabilidade(Habilidades.ID_ATACAR_UMA_POSICAO);
		    	 }else if( y > 105 && y < 140) {
		    		 habilidades.marcaHabilidade(Habilidades.ID_ATACAR_AREA_2x2);
		    	 }else if(y > 175 && y < 210 ) {
		    		 habilidades.marcaHabilidade(Habilidades.ID_ATACAR_LINHA_COLUNA);
		    	 }else if(y > 245 && y < 285) {
		    		 
		    	 }
		    }
			        
			if ((x > LARGURA_PAINEL_USER && x < (LARGURA_CELULA*map.getColunas())+LARGURA_PAINEL_USER) && (habilidades.IDHabilidadeSelecionada() != 0)) {
				 int x_pos = (x-LARGURA_PAINEL_USER)/LARGURA_CELULA;
			     int y_pos = y/ALTURA_CELULA;
			    
				if (habilidades.IDHabilidadeSelecionada() == Habilidades.ID_ATACAR_UMA_POSICAO) {
					tabuleiro.marcaCelula(y_pos, x_pos);
				}else if (habilidades.IDHabilidadeSelecionada() == Habilidades.ID_ATACAR_AREA_2x2) {
					tabuleiro.marcaCelula2x2(y_pos, x_pos);
				}else if (habilidades.IDHabilidadeSelecionada() == Habilidades.ID_ATACAR_LINHA_COLUNA) {
					tabuleiro.marcaLinhaColuna(y_pos, x_pos);
				}
			} 
		}
			
			@Override
			public void mousePressed(MouseEvent e) {
			}
			
			@Override
			public void mouseExited(MouseEvent e) {
			}
			
			@Override
			public void mouseEntered(MouseEvent e) {				
			}
			
			@Override
			public void mouseClicked(MouseEvent e) {				
			}
		});
	}
	
	public class desenhaTela extends Canvas {
		
	@Override
	public void paint(Graphics g) {
			
		final Image imgOnda = new ImageIcon("imagens/Waves/ondas.jpg").getImage();
		final Image imgShot = new ImageIcon("imagens/Actions/explosion.png").getImage();
		final Image imgTiroAgua = new ImageIcon("imagens/Actions/tiroAgua.jpg").getImage();
		final Image imgMira = new ImageIcon("imagens/Actions/mira.png").getImage();
		final Image imgDesligado = new ImageIcon("imagens/Actions/btnDesligado.png").getImage();
		final Image imgLigado = new ImageIcon("imagens/Actions/btnLigado.png").getImage();
		final Image imgJogar = new ImageIcon("imagens/Actions/btnJogar.jpg").getImage();
		final Image imgReinicar = new ImageIcon("imagens/Actions/btnReiniciar.jpg").getImage();
		final Image imgPlaca1 = new ImageIcon("imagens/Fundos/placa1.jpg").getImage();
		final Image imgPlaca2 = new ImageIcon("imagens/Fundos/placa2.jpg").getImage();
		final Image imgPlaca3 = new ImageIcon("imagens/Fundos/placa3.jpg").getImage();
		final Image imgPlaca = new ImageIcon("imagens/Fundos/placaVazia.jpg").getImage();
		final Image imgX = new ImageIcon("imagens/Fundos/X.png").getImage();
		Font serifFont = new Font("Serif", Font.BOLD, 15);
	
					for(int i = 0; i < map.getLinhas(); i++) {
						for(int j = 0; j < map.getColunas(); j++) {				
							g.drawImage(imgOnda, j*LARGURA_CELULA+TelaBatalhaNaval.LARGURA_PAINEL_USER, i*ALTURA_CELULA, LARGURA_CELULA, ALTURA_CELULA, null);
							if(tabuleiro.getMatrizClicks()[i][j] == 1) {
								if (tabuleiro.getMatrizNavios()[i][j] == 0) {
									g.drawImage(imgTiroAgua, j*LARGURA_CELULA+LARGURA_PAINEL_USER, i*ALTURA_CELULA, LARGURA_CELULA, ALTURA_CELULA, null);
								}else {
									g.drawImage(imgShot, j*LARGURA_CELULA+LARGURA_PAINEL_USER, i*ALTURA_CELULA, LARGURA_CELULA, ALTURA_CELULA, null);
								}
							}
							if ((tabuleiro.getMatrizMira()[i][j] == 1)) {// && (tabuleiro.getMatrizClicks()[i][j] == 0)) {
								g.drawImage(imgMira, j*LARGURA_CELULA+LARGURA_PAINEL_USER, i*ALTURA_CELULA, LARGURA_CELULA, ALTURA_CELULA, null);
							}
						}
					}
			
			
			
			
			g.drawImage(imgPlaca, 0, 0,TelaBatalhaNaval.LARGURA_PAINEL_USER,40, null);
			g.setFont(serifFont);
			g.setColor(Color.WHITE);
			g.drawString("Sair",20,30);
			
			g.drawImage(imgPlaca, 20, 220,160,40, null);
			g.setFont(serifFont);
		    g.setColor(Color.WHITE);
		    g.drawString(player.getNome(),35,245);
			g.drawImage(imgPlaca, 20, 270,160,40, null);
			g.setFont(serifFont);
		    g.setColor(Color.WHITE);
		    g.drawString("Creditos",35,295);
		    
		    
			
			if (habilidades.IDHabilidadeSelecionada() == Habilidades.ID_ATACAR_UMA_POSICAO) {
				g.drawImage(imgLigado, TelaBatalhaNaval.LARGURA_PAINEL_USER+(map.getColunas()*TelaBatalhaNaval.LARGURA_CELULA)+20, 20,TelaBatalhaNaval.LARGURA_BOTAO,TelaBatalhaNaval.ALTURA_BOTAO, null);
			}else {
				g.drawImage(imgDesligado, TelaBatalhaNaval.LARGURA_PAINEL_USER+(map.getColunas()*TelaBatalhaNaval.LARGURA_CELULA)+20, 20,TelaBatalhaNaval.LARGURA_BOTAO,TelaBatalhaNaval.ALTURA_BOTAO, null);
			}
			g.drawImage(imgPlaca1, TelaBatalhaNaval.LARGURA_PAINEL_USER+(map.getColunas()*TelaBatalhaNaval.LARGURA_CELULA) +  90 ,20,null);
			
			if (habilidades.IDHabilidadeSelecionada() == Habilidades.ID_ATACAR_AREA_2x2) {
				g.drawImage(imgLigado, TelaBatalhaNaval.LARGURA_PAINEL_USER+(map.getColunas()*TelaBatalhaNaval.LARGURA_CELULA)+20, 90,TelaBatalhaNaval.LARGURA_BOTAO,TelaBatalhaNaval.ALTURA_BOTAO, null);
			}else {
				g.drawImage(imgDesligado, TelaBatalhaNaval.LARGURA_PAINEL_USER+(map.getColunas()*TelaBatalhaNaval.LARGURA_CELULA)+20, 90,TelaBatalhaNaval.LARGURA_BOTAO,TelaBatalhaNaval.ALTURA_BOTAO, null);
			}
			g.drawImage(imgPlaca2, TelaBatalhaNaval.LARGURA_PAINEL_USER+(map.getColunas()*TelaBatalhaNaval.LARGURA_CELULA) + 90 ,90,null);
			
			if (habilidades.IDHabilidadeSelecionada() == Habilidades.ID_ATACAR_LINHA_COLUNA) {
				g.drawImage(imgLigado, TelaBatalhaNaval.LARGURA_PAINEL_USER+(map.getColunas()*TelaBatalhaNaval.LARGURA_CELULA)+20, 160,TelaBatalhaNaval.LARGURA_BOTAO,TelaBatalhaNaval.ALTURA_BOTAO, null);
			}else {
				g.drawImage(imgDesligado, TelaBatalhaNaval.LARGURA_PAINEL_USER+(map.getColunas()*TelaBatalhaNaval.LARGURA_CELULA)+20, 160,TelaBatalhaNaval.LARGURA_BOTAO,TelaBatalhaNaval.ALTURA_BOTAO, null);
			}
			g.drawImage(imgPlaca3, TelaBatalhaNaval.LARGURA_PAINEL_USER+(map.getColunas()*TelaBatalhaNaval.LARGURA_CELULA)+90,160,null);
			
			g.drawImage(imgDesligado, TelaBatalhaNaval.LARGURA_PAINEL_USER+(map.getColunas()*TelaBatalhaNaval.LARGURA_CELULA)+20, 230,TelaBatalhaNaval.LARGURA_BOTAO,TelaBatalhaNaval.ALTURA_BOTAO, null);
			//g.drawImage(imgPlaca1, TelaBatalhaNaval.LARGURA_PAINEL_USER+(map.getColunas()*TelaBatalhaNaval.LARGURA_CELULA)+90,20,100,60,null);
			
		}
	}
}
